package main

import (
  "diancan/dao"
  "diancan/logUtlis"
  "diancan/routers"
  "fmt"
  "github.com/iGoogle-ink/gopay"
)



func main() {
  fmt.Println("GoPay Version: ", gopay.Version)

  logUtlis.InitLog()
  dao.NewViper()
  dao.InitMysql()
  dao.InitRedis(dao.ViperConfig.GetString("redis.host")+dao.ViperConfig.GetString("redis.port"),"")
  routers := routers.InitRouter()
  routers.Static("/img", "./img/")

  routers.Run(":8080")




}
