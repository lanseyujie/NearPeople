package main

import (
	"fmt"
	"time"
)

func main() {
	ch := make(chan int)
	stopCh := make(chan bool)
	c := 0
	go Chann(ch, stopCh)

	for {
		select {
		case c = <-ch:
			fmt.Println("case1", c)
			fmt.Println("case1")
		case c := <-ch:
			fmt.Println("Receive", c)
		case _ = <-stopCh:
			goto end
		}
	}
	end:
	
}

func Chann(ch chan int, stopCh chan bool) {
	var i int
	i = 10
	for j := 0; j < 10; j++ {
		ch <- i
		time.Sleep(time.Second)
	}
	stopCh <- true
}

