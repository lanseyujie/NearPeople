package models

import "diancan/dao"

type Search struct {
	TwoListImgUrl string `json:"two_list_img_url" gorm:"column:twoListImgUrl"`
	DishName string `json:"dish_name" gorm:"column:dishname"`
	Price float32 `json:"price" gorm:"column:price"`
	EveryDay int `json:"every_day" gorm:"column:everyDay"`
}

type SearchParameter struct {
	Openid string `form:"openid"`
	Key string `form:"key"`
}

func QueryTwoMenuList(dishesName string) (mSearchRet []Search,err error)  {
	strSql:= `select twolistimgurl, dishname, price, everyday from twoMenuList  where dishName like ?`
	var mSearch []Search
	err = dao.DB.Debug().Raw(strSql, "%"+dishesName+"%").Scan(&mSearch).Error

	if err != nil {
		return nil, err
	}
	return mSearch,nil
}

