package logUtlis

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var MainLog *zap.Logger
var GatewayLog *zap.Logger

/*
初始化日志库
*/
func InitLog() {
	MainLog = NewLogger("./logs/latest.log", zapcore.InfoLevel, 128, 30, 7, true, "Main")
	GatewayLog = NewLogger("./logs/gateway.log", zapcore.DebugLevel, 128, 30, 7, true, "Gateway")
}
