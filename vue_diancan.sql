-- MySQL dump 10.13  Distrib 8.0.21, for osx10.15 (x86_64)
--
-- Host: 59.110.54.50    Database: diancan
-- ------------------------------------------------------
-- Server version	5.7.30-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `oneMenuList`
--

DROP TABLE IF EXISTS `oneMenuList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oneMenuList` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单主键id',
  `name` varchar(5) DEFAULT NULL,
  `title` varchar(10) DEFAULT NULL COMMENT 'title',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='一级菜单列表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oneMenuList`
--

LOCK TABLES `oneMenuList` WRITE;
/*!40000 ALTER TABLE `oneMenuList` DISABLE KEYS */;
INSERT INTO `oneMenuList` VALUES (1,'推荐','店家推荐'),(2,'烤肉','烤肉类'),(3,'甜点','小吃类'),(4,'茶饮','饮料类'),(5,'咖啡','咖啡系列'),(6,'套餐','套餐系列'),(7,'小吃','经典小吃');
/*!40000 ALTER TABLE `oneMenuList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storeName`
--

DROP TABLE IF EXISTS `storeName`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `storeName` (
  `name` varchar(15) DEFAULT NULL COMMENT '店名',
  `everyDay` varchar(15) DEFAULT NULL COMMENT '每日特价'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='店铺名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storeName`
--

LOCK TABLES `storeName` WRITE;
/*!40000 ALTER TABLE `storeName` DISABLE KEYS */;
INSERT INTO `storeName` VALUES ('中国美食','今日特价满100送20');
/*!40000 ALTER TABLE `storeName` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `twoMenuList`
--

DROP TABLE IF EXISTS `twoMenuList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `twoMenuList` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `twoListImgUrl` varchar(100) DEFAULT NULL COMMENT '二级列表图片路径',
  `dishName` varchar(15) DEFAULT NULL COMMENT '菜名',
  `price` float DEFAULT NULL COMMENT '菜的单价',
  `everyDay` int(11) DEFAULT NULL COMMENT '每日点餐数量',
  `oneMenuListFK` int(11) DEFAULT NULL COMMENT '一级菜单列表的外键',
  PRIMARY KEY (`id`),
  KEY `oneMenuListFK` (`oneMenuListFK`),
  CONSTRAINT `oneMenuListFK` FOREIGN KEY (`oneMenuListFK`) REFERENCES `oneMenuList` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COMMENT='二级列表菜单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `twoMenuList`
--

LOCK TABLES `twoMenuList` WRITE;
/*!40000 ALTER TABLE `twoMenuList` DISABLE KEYS */;
INSERT INTO `twoMenuList` VALUES (1,'/img/test1.png','炭烤牛肉',88,15,1),(2,'','烤牛肉',88,15,1),(3,'','烤五花肉',88,15,2),(4,'','烤生蚝',88,15,2),(5,'','烤青菜',88,15,2),(6,'','甜甜圈',46,15,3),(7,'','甜糖水',88,15,3),(8,'','烧仙草',88,15,3),(9,'','乌龙茶',46,15,4),(10,'','龙井',88,15,4),(11,'','金观音',88,15,4),(12,'','浓咖啡',46,15,5),(13,'','清香型咖啡',99,15,5),(14,'','黑咖啡',57,15,5),(15,'','牛杂煲',109,15,6),(16,'','猪肚鸡',99,15,6),(17,'','酸菜鱼',68,15,6),(18,'','金色小馒头',9,15,7),(19,'','臭豆腐',12,15,7);
/*!40000 ALTER TABLE `twoMenuList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openId` varchar(10) DEFAULT NULL,
  `userPhotoUrl` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'123456','');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-28 14:41:20
