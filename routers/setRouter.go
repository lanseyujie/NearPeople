package routers

import (
	"diancan/Controllers"
	"diancan/middleware"
	"github.com/gin-gonic/gin"
)


/*
初始化路由
*/

func InitRouter() *gin.Engine {
	r := gin.Default()

	r.Use(middleware.Cors)


	//首页
	group := r.Group("/api")
	{
		group.GET("/getMenu",Controllers.GetMenuHandel)
		group.POST("/SearchForDishes",Controllers.SearchForDishes)
		group.POST("/wxLogin",Controllers.WxLoginHandel)
		group.GET("/getAllBusinessOutlets",Controllers.GetAllBusinessOutlets)
		group.POST("/queryNear",Controllers.QueryNear)
	}

	return r
}
