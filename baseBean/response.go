package baseBean

import (
	"github.com/gin-gonic/gin"
	"net/http"
)
type BaseBeans struct {
	Code int `json:"code"`
	Msg string `json:"msg"`
	Data interface{} `json:"data"`
}



func Response(ctx *gin.Context, httpStatus int, code int, msg string, data interface{})  {
		result := &BaseBeans{
			Code: code,
			Msg:  msg,
			Data: data,
		}
		ctx.JSON(httpStatus,result)

}

func Success(ctx *gin.Context, msg string, data interface{}) {
	Response(ctx,http.StatusOK,200,msg,data)
}

func Fail(ctx *gin.Context, msg string,data interface{}) {
	Response(ctx,http.StatusOK,400,msg,data)
}


