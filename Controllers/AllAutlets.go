package Controllers

import (
	"diancan/baseBean"
	"diancan/dao"
	"diancan/models"
	"github.com/gin-gonic/gin"
)

func GetAllBusinessOutlets(c *gin.Context) {

	//客户端进来直接查询redis
	cache, err := dao.RedisMConfig.GetCache("allBusinessOutlets")
	if err != nil {
		baseBean.Fail(c,"获取所有营业网点失败,redis出错",err)
		return
	}
	if len(cache) != 0 && cache != nil {
		baseBean.Success(c,"获取成功！",cache)
		return
	}else { //查询mysql
		outlets, err := models.QueryAllBusinessOutlets()
		if err != nil {
			baseBean.Fail(c,"获取所有营业网点失败!",err)
			return
		}
		baseBean.Success(c,"获取成功！",outlets)
	}
}
