package Controllers

import (
	"diancan/baseBean"
	"diancan/dao"
	"diancan/models"
	"github.com/gin-gonic/gin"
)


func GetMenuHandel(c *gin.Context)  {
	cache, err := dao.RedisMConfig.GetCache("getMenuList")
	//redis里面如果没有取到数据`直接取数据库
	if err != nil || cache==nil{
		list, err := models.QueryMenuList()
		if err != nil {
			baseBean.Fail(c,"查询数据库出错",err)
			return
		}
		baseBean.Success(c,"查询成功",list)
		//将数据库查询出来的数据存入redis
		dao.RedisMConfig.SetCache("getMenuList",list,0)
	}else {
		//如果redis渠道数据后，直接返回给前端
		baseBean.Success(c,"查询成功",cache)
	}





}