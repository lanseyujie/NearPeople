package Controllers

import (
	"diancan/baseBean"
	"diancan/dao"
	"github.com/gin-gonic/gin"
)

func QueryNear(c *gin.Context)  {
	near := dao.RedisMConfig.QueryNear(100,50,"ASC")
	baseBean.Success(c,"获取成功",near)
}
