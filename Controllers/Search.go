package Controllers

import (
	"diancan/baseBean"
	"diancan/logUtlis"
	"diancan/models"
	"github.com/gin-gonic/gin"
)

//搜索菜品
func SearchForDishes(c *gin.Context)  {
	var mSearchParameter models.SearchParameter
	err := c.ShouldBind(&mSearchParameter)
	if err != nil {
		baseBean.Fail(c,"请求参数出错",nil)
		logUtlis.MainLog.Error("搜索菜品参数出错："+err.Error())
	}
	list, err := models.QueryTwoMenuList(mSearchParameter.Key)
	if err != nil {
		baseBean.Fail(c,"搜索失败",nil)
		logUtlis.MainLog.Error("搜索失败:"+err.Error())
		return
	}
	baseBean.Success(c,"搜索成功",list)

}
